""" 

Site Staticker
(c) Piotr Muzyczuk. 2019
https://pmuz.dev

 """
 
from jinja2 import Template, Environment, FileSystemLoader, select_autoescape
import sass
from os import makedirs, path
from shutil import copytree, rmtree, ignore_patterns
from sys import argv

input_dir = "input"
output_dir = "output"
styles_dir = "styles"

def create_output_folder():
    makedirs(output_dir, exist_ok=True)

def empty_output_folder():
    if path.isdir(output_dir):
        rmtree(output_dir, ignore_errors=False)

def render():
    with open('files.txt', 'r') as input_files:
        files = [line.strip('\n') for line in input_files.readlines()]
        if len(files) < 1:
            print("No files provided")
            return

        print("Files to generate: {}".format(files))
        print("Output folder: {}".format(output_dir))
        jinja_env = Environment(
            loader=FileSystemLoader(input_dir),
            autoescape=select_autoescape(['html','xml'])
        )

        for counter, template_filename in enumerate(files):
            template = jinja_env.get_template(template_filename)
            rendered = template.render()
            with open(path.join(output_dir,template_filename), 'w', encoding='utf-8') as output_file:
                output_file.write(rendered)
                print("{}. Generated output file: {}".format(counter+1, template_filename))

def compile_sass_styles():
    sass.compile(dirname=(path.join(input_dir,styles_dir), path.join(output_dir,styles_dir)), output_style='compressed')
    print("SASS files compiled")

def copy_data_folders():
    print('Online data folders copied' if copytree(input_dir, output_dir, ignore=ignore_patterns('*.html','styles','offline*')) else 'Error copying online folders')


if __name__ == "__main__":
    create_output_folder()
    if len(argv) > 1 and argv[1] == '1':
        empty_output_folder()
        copy_data_folders()
    compile_sass_styles()
    render()