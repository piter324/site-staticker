# SiteStaticker (SiSta)
This package provides Jinja2 with SASS compilation to render local files into static websites ready to copy to webserver.

## Mandatory directory structure
```
files.txt
input/
    styles/
```
1. `files.txt` contains names of all files that should be rendered - one per line
1. `styles/` contains all stylesheets with 'scss' extension. This folder is browsed recursively, so has to be flat.
1. `input/` directly contains all HTML files - both to be rendered and just base templates. It also contains other files required by the website, ie. folders with JavaScript, images, videos and other media.

*Please note!* A folder starting with `offline` existing in the `input` directory will not be copied to output.

## Installation and usage
Make sure you install all necessary packages from `Pipfile`, then:
```bash
python3 render.py [1]
```
Optional positional argument decides whether static files should be copied.

## Changelog
### 1.0
- Initial version
- SASS compiling
- Static file copying
- Jijna2 HTML template rendering